# Marathon Homework Repository

This repository contains the solutions for the tasks assigned during the "Marathon 1812-2912 Homework".
This README file provides information on how my homework solutions are organized.

## Homework Structure

- The repository is organized into branches for each homework task (e.g., "homework_1").
- Each branch contains folders named "day_1", "day_3", representing the specific day of the marathon.
- Inside each day folder, you will find the completed task files and the original task description (*md) file.


## Contributor

- **Name**: [Ruday Volodymyr]
- **Contact**: [https://t.me/i3oi3an]


